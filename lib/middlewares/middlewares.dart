import 'package:flutter_redux_shopping_list/repository/file_storage.dart';
import 'package:flutter_redux_shopping_list/repository/repository.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_redux_shopping_list/actions/actions.dart';
import 'package:flutter_redux_shopping_list/models/models.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> createStoreMiddleware([
  Repository repository = const Repository(
    fileStorage: const FileStorage(
      '__redux_app__',
      getApplicationDocumentsDirectory,
    ),
  ),
]) {
  final saveTodos = _createSaveShoppingItems(repository);
  final loadTodos = _createLoadShoppingItems(repository);

  return [
    TypedMiddleware<AppState, LoadShoppingItems>(loadTodos),
    TypedMiddleware<AppState, ShoppingItemsLoadedAction>(saveTodos),
    TypedMiddleware<AppState, UpdateItemQtyAction>(saveTodos),
  ];
}

Middleware<AppState> _createSaveShoppingItems(Repository repository) {
  return (Store<AppState> store, action, NextDispatcher next) {
    next(action);

    repository.saveShoppingItems(
      store.state.shoppingItems.map((item) => item.toEntity()).toList(),
    );
  };
}

Middleware<AppState> _createLoadShoppingItems(Repository repository) {
  return (Store<AppState> store, action, NextDispatcher next) {
    repository.loadShoppingItems().then(
      (items) {
        store.dispatch(
          ShoppingItemsLoadedAction(
            list: items.map(ShoppingItem.fromEntity).toList(),
          ),
        );
      },
    ).catchError((_) => store.dispatch(ShoppingItemsNotLoadedAction()));

    next(action);
  };
}
