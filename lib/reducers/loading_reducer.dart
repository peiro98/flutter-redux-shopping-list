import 'package:redux/redux.dart';
import 'package:flutter_redux_shopping_list/actions/actions.dart';

final loadingReducer = combineReducers<bool>([
  TypedReducer<bool, ShoppingItemsLoadedAction>(_setLoaded),
  TypedReducer<bool, ShoppingItemsNotLoadedAction>(_setLoaded),
]);

bool _setLoaded(bool state, action) {
  // this values does not depend on the result of the loading process
  return false;
}
