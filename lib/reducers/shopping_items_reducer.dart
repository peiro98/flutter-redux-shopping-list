import 'package:flutter_redux_shopping_list/models/models.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux_shopping_list/actions/actions.dart';

final shoppingItemsReducer = combineReducers<List<ShoppingItem>>([
  TypedReducer<List<ShoppingItem>, UpdateItemQtyAction>(_updateItem),
  TypedReducer<List<ShoppingItem>, ShoppingItemsLoadedAction>(_loadedItems),
]);

List<ShoppingItem> _updateItem(
    List<ShoppingItem> items, UpdateItemQtyAction action) {
  return items
      .map((item) => item.id == action.id ? action.item : item)
      .toList();
}

List<ShoppingItem> _loadedItems(
    List<ShoppingItem> items, ShoppingItemsLoadedAction action) {
  return action.list;
}
