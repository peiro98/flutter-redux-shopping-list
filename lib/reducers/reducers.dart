import 'package:flutter_redux_shopping_list/models/models.dart';
import 'loading_reducer.dart';
import 'shopping_items_reducer.dart';

AppState appReducer(AppState state, action) {
  return AppState(
    isLoading: loadingReducer(state.isLoading, action),
    shoppingItems: shoppingItemsReducer(state.shoppingItems, action),
  );
}
