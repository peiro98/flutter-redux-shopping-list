import 'dart:async';

import 'package:flutter_redux_shopping_list/repository/entities/shopping_item_entity.dart';
import 'package:uuid/uuid.dart';

class WebClient {
  final Duration delay;

  const WebClient([this.delay = const Duration(milliseconds: 3000)]);

  Future<List<ShoppingItemEntity>> fetchShoppingItems() async {
    var uuid = new Uuid();
    return Future.delayed(
        delay,
        () => [
              new ShoppingItemEntity(
                id: uuid.v4(),
                name: 'Waffles',
                quantity: 0,
                image:
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Waffles_with_Strawberries.jpg/1280px-Waffles_with_Strawberries.jpg',
              ),
              new ShoppingItemEntity(
                id: uuid.v4(),
                name: 'Fragole',
                quantity: 1,
                image:
                    'https://cdn.pixabay.com/photo/2017/09/04/15/41/strawberry-2714444_960_720.jpg',
              ),
              new ShoppingItemEntity(
                id: uuid.v4(),
                name: 'Pane',
                quantity: 2,
                image:
                    'https://image.freepik.com/vrije-photo/zuurdesem-gist-zure-brood-eten_1203-5439.jpg',
              ),
              new ShoppingItemEntity(
                id: uuid.v4(),
                name: 'Pomodoro',
                quantity: 4,
                image:
                    'https://cdn.pixabay.com/photo/2014/04/02/10/43/tomato-304316_960_720.png',
              ),
            ]);
  }
}
