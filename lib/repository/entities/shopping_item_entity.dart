import 'package:meta/meta.dart';
import 'package:uuid/uuid.dart';

var uuid = new Uuid();

class ShoppingItemEntity {
  final String id;
  final String name;
  final int quantity;
  final String image;

  ShoppingItemEntity(
      {@required this.id,
      @required this.name,
      @required this.quantity,
      @required this.image});

  @override
  String toString() {
    return 'ShoppingItemEntity(id: $id, name: $name, quantity: $quantity, image: $image)';
  }

  @override
  int get hashCode =>
      id.hashCode ^ name.hashCode ^ quantity.hashCode ^ image.hashCode;

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        other is ShoppingItemEntity &&
            runtimeType == other.runtimeType &&
            id == other.id &&
            name == other.name &&
            quantity == other.quantity &&
            image == other.image;
  }

  Map<String, Object> toJson() {
    return {
      "id": id,
      "name": name,
      "quantity": quantity,
      "image": image,
    };
  }

  static ShoppingItemEntity fromJson(Map<String, Object> json) {
    return ShoppingItemEntity(
      id: json["id"] as String,
      name: json["name"] as String,
      quantity: json["quantity"] as int,
      image: json["image"] as String,
    );
  }
}
