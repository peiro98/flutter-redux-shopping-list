import 'dart:async';
import 'dart:core';

import 'package:flutter_redux_shopping_list/repository/entities/shopping_item_entity.dart';
import 'package:flutter_redux_shopping_list/repository/file_storage.dart';
import 'package:flutter_redux_shopping_list/repository/web_client.dart';
import 'package:meta/meta.dart';

class Repository {
  final FileStorage fileStorage;
  final WebClient webClient;

  const Repository({
    @required this.fileStorage,
    this.webClient = const WebClient(),
  });

  Future<List<ShoppingItemEntity>> loadShoppingItems() async {
    try {
      return await fileStorage.loadShoppingItems();
    } catch (e) {
      final todos = await webClient.fetchShoppingItems();

      fileStorage.saveShoppingItems(todos);

      return todos;
    }
  }

  Future saveShoppingItems(List<ShoppingItemEntity> todos) {
    return Future.wait<dynamic>([
      fileStorage.saveShoppingItems(todos),
    ]);
  }
}
