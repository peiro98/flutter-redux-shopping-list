import 'package:flutter/material.dart';
import 'package:flutter_redux_shopping_list/containers/containers.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen() : super(key: const Key('__homescreen__'));

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Redux shopping list'),
      ),
      body: ShoppingList(),
    );
  }
}
