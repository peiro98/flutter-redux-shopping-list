import 'package:flutter/material.dart';
import 'package:flutter_redux_shopping_list/models/models.dart';

class ShoppingListView extends StatelessWidget {
  final List<ShoppingItem> items;
  final Function(ShoppingItem, int) onUpdateQty;

  ShoppingListView({@required this.items, @required this.onUpdateQty});

  Widget _buildRow(ShoppingItem item) {
    final TextStyle _fontStyle = new TextStyle(
      fontSize: 16.0,
      color: Colors.black,
    );
    return new ListTile(
      title: new Text(
        '${item.name} (${item.quantity})',
        style: _fontStyle,
      ),
      leading: Image.network(
        item.image,
        width: 32.0,
        height: 32.0,
      ),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          IconButton(
            icon: Icon(
              Icons.keyboard_arrow_up,
              color: Colors.green,
              size: 24.0,
            ),
            onPressed: () => this.onUpdateQty(item, 1),
          ),
          IconButton(
            icon: Icon(
              Icons.keyboard_arrow_down,
              color: item.quantity == 0 ? Colors.grey : Colors.red,
              size: 24.0,
            ),
            onPressed:
                item.quantity == 0 ? null : () => this.onUpdateQty(item, -1),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(8.0),
      itemBuilder: (context, i) {
        if (i.isOdd) return Divider();

        final index = i ~/ 2;
        if (index < items.length) {
          return _buildRow(items[index]);
        }
      },
    );
  }
}
