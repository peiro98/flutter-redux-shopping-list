import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_shopping_list/middlewares/middlewares.dart';
import 'package:flutter_redux_shopping_list/views/presentations.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux_shopping_list/models/models.dart';
import 'package:flutter_redux_shopping_list/routes/routes.dart';
import 'package:flutter_redux_shopping_list/actions/actions.dart';
import 'package:flutter_redux_shopping_list/reducers/reducers.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  final store = Store<AppState>(
    appReducer,
    initialState: AppState.loading(),
    middleware: createStoreMiddleware(),
  );

  final theme = new ThemeData(
    primarySwatch: Colors.blue,
  );

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        title: 'Flutter Redux shopping list',
        theme: theme,
        routes: {
          Routes.home: (context) {
            return StoreBuilder<AppState>(
              onInit: (store) {
                store.dispatch(LoadShoppingItems());
              },
              builder: (context, store) {
                return HomeScreen();
              },
            );
          },
        },
      ),
    );
  }
}
