import 'package:meta/meta.dart';
import 'shopping_item.dart';

@immutable
class AppState {
  final bool isLoading;
  final List<ShoppingItem> shoppingItems;

  AppState({this.isLoading, this.shoppingItems = const []});

  factory AppState.loading() => AppState(isLoading: true);

  AppState copyWith({isLoading, shoppingItems}) {
    return AppState(
      isLoading: isLoading ?? this.isLoading,
      shoppingItems: shoppingItems ?? this.shoppingItems,
    );
  }

  @override
  int get hashCode => this.shoppingItems.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState && shoppingItems == other.shoppingItems;
}
