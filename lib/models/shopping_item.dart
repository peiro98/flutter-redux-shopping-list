import 'package:flutter_redux_shopping_list/repository/entities/shopping_item_entity.dart';
import 'package:uuid/uuid.dart';

var uuid = new Uuid();

class ShoppingItem {
  final String id;
  final String name;
  final int quantity;
  final String image;

  ShoppingItem(this.name, {this.quantity = 0, this.image, String id})
      : this.id = new Uuid().v4();

  ShoppingItem copyWith({id, name, quantity, image}) {
    return ShoppingItem(
      name ?? name,
      id: id ?? this.id,
      quantity: quantity ?? this.quantity,
      image: image ?? this.image,
    );
  }

  @override
  String toString() {
    return 'ShoppingItem(id: ${this.id}, name: ${this.name}, quantity: ${this.quantity}, image: ${this.image})';
  }

  @override
  int get hashCode =>
      id.hashCode ^ name.hashCode ^ quantity.hashCode ^ image.hashCode;

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        other is ShoppingItem &&
            runtimeType == other.runtimeType &&
            id == other.id &&
            name == other.name &&
            quantity == other.quantity &&
            image == other.image;
  }

  ShoppingItemEntity toEntity() {
    return ShoppingItemEntity(
      id: id,
      name: name,
      quantity: quantity,
      image: image,
    );
  }

  static ShoppingItem fromEntity(ShoppingItemEntity entity) {
    return ShoppingItem(
      entity.name,
      quantity: entity.quantity ?? 0,
      image: entity.image ?? '',
      id: entity.id ?? Uuid().v4(),
    );
  }
}
