import 'package:flutter_redux_shopping_list/models/models.dart';
import 'package:meta/meta.dart';

class LoadShoppingItems {}

class ShoppingItemsLoadedAction {
  final List<ShoppingItem> list;

  ShoppingItemsLoadedAction({
    @required this.list,
  });
}

class ShoppingItemsNotLoadedAction {}

class UpdateItemQtyAction {
  final String id;
  final ShoppingItem item;

  UpdateItemQtyAction({
    @required this.id,
    @required this.item,
  });

  @override
  String toString() {
    return 'UpdateItemQty({id: ${this.id}, item: ${this.item})';
  }
}
