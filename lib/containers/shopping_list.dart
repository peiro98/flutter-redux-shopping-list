import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_shopping_list/actions/actions.dart';
import 'package:flutter_redux_shopping_list/models/models.dart';
import 'package:flutter_redux_shopping_list/views/shopping_list_view.dart';
import 'package:redux/redux.dart';

class ShoppingList extends StatelessWidget {
  ShoppingList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (context, vm) {
        return ShoppingListView(
          items: vm.items,
          onUpdateQty: vm.onUpdateItemQty,
        );
      },
    );
  }
}

class _ViewModel {
  final List<ShoppingItem> items;
  final bool loading;
  final Function(ShoppingItem, int) onUpdateItemQty;

  _ViewModel(
      {@required this.items,
      @required this.loading,
      @required this.onUpdateItemQty});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      items: store.state.shoppingItems,
      loading: store.state.isLoading,
      onUpdateItemQty: (item, value) {
        store.dispatch(
          UpdateItemQtyAction(
            id: item.id,
            item: item.copyWith(
              quantity: item.quantity + value,
            ),
          ),
        );
      },
    );
  }
}
